#if !defined HATP_MAKERESULT_HH_
#define HATP_MAKERESULT_HH_

// Answer for planning failure
static string makeMsgError(string id) {
	ostringstream result;
	result << "{\"HATP-REP\": {\"REQ-ID\": " << id << ", \"REPORT\": \"FAIL\", ";
	result << "\"NODES\": [ ], \"STREAMS\": null, \"TREE\": null }}";
	return result.str();
}

// Answer for plan already exists
static string makeMsgAlready(string id) {
	ostringstream result;
	result << "{\"HATP-REP\": {\"REQ-ID\": " << id << ", \"REPORT\": \"ACHIEVED\", ";
	result << "\"NODES\": [ ], \"STREAMS\": null, \"TREE\": null }}";
	return result.str();
}

// Answer for plans found or no plan
static string makeMsgResult(Plan *p, string id) {
	ostringstream result;
	result << "{\"HATP-REP\": {\"REQ-ID\": " << id << ", ";
	if(!p) result << "\"REPORT\": \"NOPLAN\", \"NODES\": [ ], \"STREAMS\": null, \"TREE\": null ";
	else {
		result << "\"REPORT\": \"OK\", \"NODES\": [";
		result << "";
		bool moreThanOne;
		vector<unsigned int> rootStreams, rootTree;
		vector<TaskListNode*> tlnodes = p->getProjection().getNodes();
		set<unsigned int> alreadyIn;
		vector<PlanTreeTaskListNode*> ptnodes = p->getTree().getRootList()->getAllNodes();
		vector<PlanTreeTaskListNode*> ptrnodes = p->getTree().getRootList()->getNodes();
		for(size_t i = 0 ; i < ptrnodes.size() ; i++) rootTree.push_back(ptrnodes[i]->getID());
		for(size_t i = 0 ; i < ptnodes.size() ; i++) {
			TaskInstance task = ptnodes[i]->getInstance();
			alreadyIn.insert(ptnodes[i]->getID());
			result << "{\"NODE\": { \"NODE-ID\": " << ptnodes[i]->getID() << ", ";
			if(task.isAction()) result << "\"NODE-TYPE\": \"" << "action" << "\", ";
			else result << "\"NODE-TYPE\": \"" << "metatask" << "\", ";
			result << "\"ACTION-NAME\": \"" << task.getGeneralTask().getName() << "\", ";
			vector<string> agents = ptnodes[i]->getInvolvedAgents();
			result << "\"AGENTS\": [";
			for(size_t k = 0 ; k < agents.size() ; k++) {
				if(k != 0) result << ", ";
				result << "\""<< agents[k] << "\"";
			}
			result << "], "; 
			if(task.isAction()) {
				pair<double, double> wtime = p->getTimeProjection()->getTimeWindow(ptnodes[i]->getID());
				result << "\"START-TIME\": " << wtime.first << ", ";
				result << "\"END-TIME\": " << wtime.second << ", ";
			}
			else {
				result << "\"START-TIME\": " << "0" << ", ";
				result << "\"END-TIME\": " << "0" << ", ";			
			}
			result << "\"PARAMETERS\": [ ";
			vector<string> args = task.getArgs();
			for(size_t j = 0 ; j < args.size() ; j++) {
				if(j != 0) result << ", ";
				result << "\""<< args[j] << "\"";
			}
			result << "]"; // parameters
			result << "}}"; // action		
		}
		// find stream roots
		for(size_t i = 0 ; i < tlnodes.size() ; i++) {
			if(tlnodes[i]->noPredecessors()) rootStreams.push_back(tlnodes[i]->getID()); 
		}		
		// adding commActions ...
		for(size_t i = 0 ; i < tlnodes.size() ; i++) {
			if(alreadyIn.find(tlnodes[i]->getID()) == alreadyIn.end()) {
				TaskInstance task = tlnodes[i]->getInstance();
				result << "{\"NODE\": { \"NODE-ID\": " << tlnodes[i]->getID() << ", ";
				result << "\"NODE-TYPE\": \"" << "action" << "\", ";
				result << "\"ACTION-NAME\": \"" << task.getGeneralTask().getName() << "\", ";
				vector<string> agents = task.getAssociatedAgents();
				result << "\"AGENTS\": [";
				for(size_t k = 0 ; k < agents.size() ; k++) {
					if(k != 0) result << ", ";
					result << "\""<< agents[k] << "\"";
				}
				result << "], "; 
				if(task.isAction()) {
					pair<double, double> wtime = p->getTimeProjection()->getTimeWindow(tlnodes[i]->getID());
					result << "\"START-TIME\": " << wtime.first << ", ";
					result << "\"END-TIME\": " << wtime.second << ", ";
				}
				else {
					result << "\"START-TIME\": " << "0" << ", ";
					result << "\"END-TIME\": " << "0" << ", ";			
				}
				result << "\"PARAMETERS\": [ ";
				vector<string> args = task.getArgs();
				for(size_t j = 0 ; j < args.size() ; j++) {
					if(j != 0) result << ", ";
					result << "\""<< args[j] << "\"";
				}
				result << "]"; // parameters
				result << "}}"; // action		
			}
		}
		result << "], "; // nodes
		result << "\"STREAMS\": { \"ROOTS\": [";
		moreThanOne = false;
		for(size_t i = 0 ; i < rootStreams.size() ; i++) {
			if(moreThanOne) result << ", ";
			result << rootStreams[i];
			moreThanOne = true;		
		}
		result << "], \"LINKS\": [";
		moreThanOne = false;
		for(size_t i = 0 ; i < tlnodes.size() ; i++) {
			vector<TaskListNode*> vec = tlnodes[i]->getSuccessors();
			for(size_t j = 0 ;  j < vec.size() ; ++j) {
				if(moreThanOne) result << ", ";
				result << "{\"LINK\": { \"TYPE\": \"causal\"";
				result << ", \"FROM\": " << tlnodes[i]->getID() << ", \"TO\": " << vec[j]->getID() << "}}";
				moreThanOne = true;
			}
		}
		result << "] }, "; // links / streams
		result << "\"TREE\": { \"ROOTS\": [";
		moreThanOne = false;
		for(size_t i = 0 ; i < rootTree.size() ; i++) {
			if(moreThanOne) result << ", ";
			result << rootTree[i];
			moreThanOne = true;		
		}
		result << "], \"LINKS\": [";
		moreThanOne = false; 
		for(size_t i = 0 ; i < ptnodes.size() ; i++) {
			vector<PlanTreeTaskListNode*> vec = ptnodes[i]->getSuccessors(); 
			for(size_t j = 0 ;  j < vec.size() ; ++j) { 
				if(moreThanOne) result << ", "; 
				result << "{\"LINK\": { \"TYPE\": \"causal\"";
				result << ", \"FROM\": " << ptnodes[i]->getID() << ", \"TO\": " << vec[j]->getID() << "}}"; 
				moreThanOne = true;
			}
		}
		vector<pair<unsigned int, unsigned int> > hlinks = p->getTree().getRootList()->getHierarchicalLinks();
		for(size_t j = 0 ; j < hlinks.size() ; j++) {
			if(moreThanOne) result << ", "; 
			result << "{\"LINK\": { \"TYPE\": \"hierarchical\"";
			result << ", \"FROM\": " << hlinks[j].first << ", \"TO\": " << hlinks[j].second << "}}"; 
			moreThanOne = true;
		}
		result << "] } "; // links / tree
	}
	result << "}}"; // hatp-rep
	return result.str();
}

// Answer for get all tasks (goals)
static string getAllTasks(HTNBase& htn) {
	ostringstream result;
	result << "{\"HATP-REP\": {\"RESULT\": [";
	map<string, Task*> db = htn.getDatabase();
	bool isFirstTask = true;
	for(map<string, Task*>::iterator it = db.begin() ; it!= db.end() ; it++) {
		if(!(*it).second->isAction()) {
			if(!isFirstTask) result << ", ";
			else isFirstTask = false;
			result << "{\"TASK-NAME\": \"" << (*it).second->getName() << "\", ";
			result << "\"PARAMETERS\": [";
			vector<string> args = (*it).second->getArgTypes();
			bool isFirstParam = true;
			for(size_t i = 0 ; i < args.size() ; i++) {
				if(!isFirstParam) result << ", ";
				else isFirstParam = false;
				result << "\"" << args[i] << "\"";
			}
			result << "]}";
		}
	}	
	result << "]}}";
	return result.str();
}

// Answer for get all actions (primitive tasks)
static string getAllActions(HTNBase& htn) {
	ostringstream result;
	result << "{\"HATP-REP\": {\"RESULT\": [";
	map<string, Task*> db = htn.getDatabase();
	bool isFirstTask = true;
	for(map<string, Task*>::iterator it = db.begin() ; it!= db.end() ; it++) {
		if((*it).second->isAction()) {
			if(!isFirstTask) result << ", ";
			else isFirstTask = false;
			result << "{\"TASK-NAME\": \"" << (*it).second->getName() << "\", ";
			result << "\"PARAMETERS\": [";
			vector<string> args = (*it).second->getArgTypes();
			bool isFirstParam = true;
			for(size_t i = 0 ; i < args.size() ; i++) {
				if(!isFirstParam) result << ", ";
				else isFirstParam = false;
				result << "\"" << args[i] << "\"";
			}
			result << "]}";
		}
	}	
	result << "]}}";
	return result.str();
}

static string getAllEntities(WorldBase& wb) {
	ostringstream result;
	const std::map<std::string, Entity*>& entities(wb.getDatabase());
	std::map<std::string, Entity*>::const_iterator it;

	result<<"{\"HATP-REP\": {\"RESULT\": [";
	bool isFirstEntity=true;	
	for(it=entities.begin(); it!=entities.end(); ++it){
		if(!isFirstEntity) result<<", ";
		else isFirstEntity=false;
		result<<"{\"ENTITY\": \""<<it->first<<"\", ";
		result<<"\"TYPE\": \""<<it->second->getType()<<"\"}";
	}
	result<<"]}}";
	return result.str();
}


#endif
