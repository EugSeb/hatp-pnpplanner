# Find the msgconnector module
# Once found the following variables are set :
#	msgconnector_FOUND	True to specify that the module has been found
#
#	msgconnector_INCLUDE_DIRS	Path(s) to the headers
#
#	msgconnector_LIBRARIES	Link these to use msgconnector

# To help CMake in finding the msgconnector module
# one can set the following variables :
# msgconnector_DIR	Those variables can be either set using 'set' command of
# or			CMake, or using '-D<var>=value' while calling CMake or 
# msgconnector_ROOT	finally they can be set as environment variables.
# or			Those three variables are aliases, so just set one is enough.
# msgconnectorROOT
#
# You can set any of them using one of the following methods :
#	1) Set it as environnement varialbe (e.g. export)
#	2) Give it as argument for cmake : cmake -Dmsgconnector_DIR="...."
#	3) In a CMakeLists.txt, set a variable : set(msgconnector_DIR ...)

#Varaibles to change if the module changes
set(HEADER_FILES iclient.hh imsg.hh message.hh msgClient.hh msgConstants.hh)
set(INCLUDE_PATH_SUFFIXES include include/msgconnector msgconnector)
set(LIBRARIES libmsgconnectorClient.so)

foreach(header ${HEADER_FILES})
	find_path(${header}_PATH ${header} PATH_SUFFIXES ${INCLUDE_PATH_SUFFIXES}
	HINTS ${msgconnector_DIR} ${msgconnector_ROOT} ${msgconnectorROOT}
	$ENV{msgconnector_DIR} $ENV{msgconnector_ROOT} $ENV{msgconnectorROOT})
	if(${header}_PATH)
		list(APPEND msgconnector_INCLUDE_DIRS ${${header}_PATH})
		unset(${header}_PATH CACHE)
	else(${header}_PATH)
		list(APPEND MISSING_HEADERS ${header})
	endif(${header}_PATH)
endforeach()

if(msgconnector_INCLUDE_DIRS)
	list(REMOVE_DUPLICATES msgconnector_INCLUDE_DIRS)
endif(msgconnector_INCLUDE_DIRS)

if(MISSING_HEADERS)
	unset(msgconnector_INCLUDE_DIRS)
endif(MISSING_HEADERS)

find_file(msgconnector_LIBRARIES ${LIBRARIES} PATH_SUFFIXES lib 
	HINTS ${msgconnector_DIR} ${msgconnector_ROOT} ${msgconnectorROOT}
	$ENV{msgconnector_DIR} $ENV{msgconnector_ROOT} $ENV{msgconnectorROOT})

if(msgconnector_INCLUDE_DIRS AND msgconnector_LIBRARIES)
	set(msgconnector_FOUND TRUE)
else(msgconnector_INCLUDE_DIRS AND msgconnector_LIBRARIES)
	if(NOT msgconnector_FIND_QUIETLY)
		if(NOT msgconnector_INCLUDE_DIRS)
			message(STATUS "Unable to find msgconnector, the following header files are missing : ${MISSING_HEADERS}")
		else(NOT msgconnector_INCLUDE_DIRS)
			message(STATUS "Header files found.")
		endif(NOT msgconnector_INCLUDE_DIRS)
		if(NOT msgconnector_LIBRARIES)
			message(STATUS "Unable to find msgconnector library files!")
		else(NOT msgconnector_LIBRARIES)
			message(STATUS "Library files found.")
		endif(NOT msgconnector_LIBRARIES)
	endif(NOT msgconnector_FIND_QUIETLY)
endif(msgconnector_INCLUDE_DIRS AND msgconnector_LIBRARIES)

if(msgconnector_FOUND)
	if(NOT msgconnector_FIND_QUIETLY)
		message(STATUS "Found components for msgconnector")
		message(STATUS "msgconnector_INCLUDE_DIRS = ${msgconnector_INCLUDE_DIRS}")
		message(STATUS "msgconnector_LIBRARIES = ${msgconnector_LIBRARIES}")
	endif(NOT msgconnector_FIND_QUIETLY)
else(msgconnector_FOUND)
	if(msgconnector_FIND_REQUIRED)
		set(msgconnector_DIR "" CACHE PATH "Paths where to additionally look for
		msgconnector (alias of msgconnector_ROOT and msgconnectorROOT)")
		set(msgconnector_ROOT "" CACHE PATH "Paths where to additionally look for
		msgconnector (alias of msgconnector_DIR and msgconnectorROOT)")
		set(msgconnectorROOT "" CACHE PATH "Paths where to additionally look for
		msgconnector (alias of msgconnector_DIR and msgconnector_ROOT)")
		if(NOT msgconnector_FIND_QUIETLY)
			message("\nCould not find msgconnector!\n")
			message("You can use any of the following variables to help CMake to find the header files and libraries :")
			message("	msgconnector_DIR or msgconnector_ROOT or msgconnectorROOT")
			message("You should use one and set it to the root path of your local msgconnector install directory (if you installed it in a different folder than system root).")
			message("To set one you can either :")
			message("	- use 'set(msgconnector_DIR ....)' command in the CMakeLists.txt (separator ' ')")
			message("	- use '-Dmsgconnector_DIR=....' as an option to Cmake (separator ';')")
			message("	- set one as an environment variable (e.g. export in bash, with separator ';')\n")
		endif(NOT msgconnector_FIND_QUIETLY)
		message(FATAL_ERROR "Could not find msgconnector!")
	endif(msgconnector_FIND_REQUIRED)
endif(msgconnector_FOUND)

mark_as_advanced (
	msgconnector_FOUND
	msgconnector_LIBRARIES
	msgconnector_INCLUDE_DIRS
)
