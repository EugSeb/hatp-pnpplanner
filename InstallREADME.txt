git clone git://git.openrobots.org/robots/hatp/msgconnector
git clone git://git.openrobots.org/robots/hatp/hatponboard-lib
git clone git://git.openrobots.org/robots/hatp/libhatp
git clone git://git.openrobots.org/robots/hatp/hatpconsole.git
git clone git://git.openrobots.org/robots/hatp/hatptester.git
git clone git://git.openrobots.org/robots/hatp/hatponboard


mkdir /opt/openrobots2
sudo chown -R eugenio.eugenio /opt/openrobots2

sudo apt-get install libantlr-dev antlr antlr-doc
sudo apt-get install libgraphviz-dev
sudo apt-get install libjsoncpp-dev


1. msgconnector

cd msgconnector
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/opt/openrobots2 -DCOMPILE_BRIDGE_PRINT=ON
make
make install



2. hatponboard-lib use checkout 30/11/2015

git log
git checkout <IDCOMMIT30-11-2015>
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/opt/openrobots2
make
make install



3. libhatp

mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/opt/openrobots2
make
make install



4. hatpconsole use checkout 15/10/2015

git log
git checkout <IDCOMMIT15-10-2015>

mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/opt/openrobots2 -DCMAKE_MODULE_PATH=/opt/openrobots2/share/cmake/Modules/share/cmake/Modules -DmsgconnectorROOT=/opt/openrobots2 -DlibhatpROOT=/opt/openrobots2
make 
make install



5. hatptester use checkout 10/12/2015

git log
git checkout <IDCOMMIT10-12-2015>

mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/opt/openrobots2 -DCMAKE_MODULE_PATH=/opt/openrobots2/share/cmake/Modules/share/cmake/Modules -DmsgconnectorROOT=/opt/openrobots2
make
make install


Non usato: -DENABLE_LOGGER=ON -Dboost-logROOT=<boost-log/root/directory>


6. hatponboard use checkout 20/1/2016

git log
git checkout <IDCOMMIT20-1-2016>

mkdir build
mkdir generated
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/opt/openrobots2
cd ..
export HATP_BASE=$HOME/src/HATP/hatponboard2 %This two export can be done in bashrc file
export PATH=$PATH:/opt/openrobots2/bin
./parse
./run

