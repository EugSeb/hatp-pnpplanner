
#include<time.h>
#include<signal.h>
#include<iostream>
#include<fstream>
#include<sstream>
#include<map>
#include<set>
#include<string>

using namespace std;

// Boost
#include<boost/regex.hpp>
#include<boost/date_time/posix_time/posix_time.hpp>
#include<boost/thread/thread.hpp>
#include<boost/lexical_cast.hpp>
#include<boost/program_options.hpp>

using namespace boost;
using namespace boost::posix_time;

// Tools
#include <wait.hh>
#include <tools.hh>
#include <PlanSender.hh>
#include <MainSessionMsg.hh>

using namespace HATP_TOOLS;

// Connector
#include <msgClient.hh>

// PlanningSession
#include <ChoicePoint.hh>
#include <ChoiceReminder.hh>
#include <Task.hh>
#include <HTNBase.hh>
#include <Entity.hh>
#include <WorldBase.hh>
#include <WorldBaseReport.hh>
#include <PlanStockManager.hh>
#include <hatpPlanningSession.hh>
#include <LightEntity.hh>
#include <LightWorldBase.hh>
#include <PartialPlanAppraiser.hh>
#include <SocialRuleBase.hh>

// Plan
#include <Plan.hh>
#include <PlanTree.hh>
#include <PlanTreeTaskList.hh>
#include <PlanTreeTaskListNode.hh>
#include <TaskInstance.hh>
#include <TaskListNode.hh>
#include <TaskList.hh>
#include <Substitution.hh>
#include <SocialRuleData.hh>
#include <UndesirableStateData.hh>
#include <WastedTimeData.hh>

// ---------------------------------

#include "requestLexer.hh"
#include "makeResult.hh"

// ---------- ---------- ----------

// //Meded to trasform HATP in a PNP
 #include "pnpgenerator.h"
 //#include "msgClient.hh"
 #include "hatpLexer.hh"
 #include "hatpNode.hh"
 #include "hatpPlan.hh"
 #include "hatpStreams.hh"
 #include "hatpTaskList.hh"
 #include "hatpTools.hh"
 #include "hatpTree.hh"

 // ---------- ---------- ----------

//WorldBase Updater from ontology
#include <WorldBaseUpdater.hh>

static const string THIS_NAME = "HATP";
static const string CONS_NAME = "hatpconsole";

static bool STANDALONE_VERSION = true;
static bool LIMIT_TIME = false;
static int MAX_PLANNING_TIME = 3600000;
//static int REQ_ORO_COUNT = 0;

static string ONTO_NAME = "";
static bool ONTO_TYPE = true; //false = ORO; true = OpenPRS

static unsigned short VERBOSE_LEVEL = 0; 
//0 minimal message output, 1 few message output, 2 some message output, 3 maximum message output
//In detail : 1 = messages about request (planning, replanning) and basic information (max time, ... )
// 2 = messages about internal state (most of which can be found in hatpconsole)
// 3 = messages about internal state but that require lots of space to output

extern int numberOfActions = 0;

#include <VerboseLevel.hh>
//Defines three levels VERBOSE_1, VERBOSE_2, VERBOSE_3

WorldBase wb_init;
LightWorldBase wb_light;
//TODO replace by a proper function that protects constness
HTNBase htn_base;
SocialRuleBase srb;
PartialPlanAppraiser ppa;
PlanStockManager stock;

// Generated 
#include "initWorldBase.cc"

//The symbol USE_GTP_PLANNER is defined if 'additionalPlanner { GTP { ... }; }' appears in the domain file
#ifdef USE_GTP_PLANNER
#include <ILGTP.hh>
#endif //USE_GTP_PLANNER

#include "initHTNBase.cc"
#include "initSocialRuleBase.cc"

#ifdef USE_ASSEMBLY_PLANNER
#include <ILAP.hh>
#endif //USE_ASSEMBLY_PLANNER

msgClient mp;


// ---------- ---------- ----------

void mainSessionPlanning(TaskList& problem, PlanTree& problemPT, WorldBase& worldBase, bool time_limit, unsigned int duration, bool stopAtFirstPlan) {
  stock.clear();
  ppa.init();
  srb.init();
  PlanSender<MainSessionMsg> planSender;
  boost::thread sessionThread(hatpPlanningSession(worldBase, htn_base, srb, problem, problemPT, ppa, &planSender, time_limit, duration, stopAtFirstPlan, VERBOSE_LEVEL));
  ptime estimated_end = microsec_clock::local_time() + microsec(duration*1000);
  stock.setStartTime(microsec_clock::local_time());
  pair<bool,bool> waitResult;
  bool exit=false;
  while(!exit) { 
    if(time_limit)
      waitResult = waitBySelect( planSender.fdToWaitB(), (estimated_end-microsec_clock::local_time()).total_milliseconds()/1000.);
    else {
      waitBySelect(planSender.fdToWaitB());
      waitResult = make_pair(true, false);
    }
    if( waitResult.first && (!time_limit || microsec_clock::local_time() < estimated_end) ) {
      switch(planSender.typeOfMessageFromB()) {
        case End : 
          sessionThread.join();
          exit = true; break;
        case GetBestScore : 
          if(planSender.objectAvailableFromB()) {
            planSender.receiveObjectFromB<string>(); // content not used
            planSender.sendObjectToB(stock.getBestPlanDetails(), BestScore);
          } break;
        case NewPlan : 
          if( planSender.objectAvailableFromB() ) {
            stock.addPlan(planSender.receiveObjectFromB<Plan*>());
            if(stock.getLastMessage() != "") mp.sendMessage(CONS_NAME, stock.getLastMessage());
          } break;
        case SolFound :
          stock.setSolutionExists(true);
          break;
        default : ;
      } // end switch
    } // end waitResult && (!time_limit || time < estimated_time)
    else { // we have reached the dealine
      while(planSender.objectAvailableFromB()) {
        switch(planSender.typeOfMessageFromB()) {
          case GetBestScore :
          case End : planSender.receiveObjectFromB<string>(); break;
          case NewPlan :delete planSender.receiveObjectFromB<Plan*>(); break;
          default : ;
        }
      }
      exit = true;
      sessionThread.join();
    } // end we have reached the deadline
  } // end while(!exit)
  ppa.init();
}

// void launchPlanningSession(string rqid, string task, vector<string> params, string sender, bool stopAtFristPlan, WorldBaseUpdater& wbu) {
//   if(htn_base.existingTask(task)) {
// //    WorldBase wb(wb_init);
//     bool updated = false;
    
//     #ifdef USE_ASSEMBLY_PLANNER
//     bool firstRequest = true;
//     bool planFound = false;
//     bool noMoreAPPlan = false;
//     while(!planFound && !noMoreAPPlan){
//     #endif //USE_ASSEMBLY_PLANNER
//     WorldBase wb(wb_init);
//     if(htn_base.find(task).checkTypes(params, wb)) {
//       if(!STANDALONE_VERSION) { // with ontology version
//         timeval tim;
//         gettimeofday(&tim, NULL);
//         double t1=tim.tv_sec+(tim.tv_usec/1000000.0);
//         if(ONTO_TYPE==false){ //Onotlogy type = ORO
//           if(wb.isActiveBM()) updated = wbu.updateBMWorldBaseFromOntology(wb);
//           else updated = wbu.updateWorldBaseFromOntology(wb) ;
//         }else{ //Onotlogy type = OpenPRS
//           if(wb.isActiveBM()) updated = wbu.updateWholeBMWorldBaseFromOntology(wb);
//           else updated = wbu.updateWholeWorldBaseFromOntology(wb) ;
//         }
//         gettimeofday(&tim, NULL);
//         double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
//         ostringstream res;
//         res << "WorldBase updated in " << (t2 - t1)*1000 << " ms.";
//         VERBOSE_1 << res.str() << endl;
//         mp.sendMessage(CONS_NAME, res.str());
//       }       
//       else updated = true; // standalone version
      
//       //Make any Intermediate layer ready for this new planning
//       #ifdef USE_GTP_PLANNER
//       if(!ILGTP::getInstance().initGTP(wb)){
//         cerr << "Failed to reset the environment" << endl;
//         updated=false;
//       }
//       #endif //USE_GTP_PLANNER
      
//       #ifdef USE_ASSEMBLY_PLANNER
//       if(params.size()!=1){
//         cerr << "When calling HATP with the AssemblyPlanner intermediate layer, there must be only one parameter to the request" << endl;
//         updated=false;
//       }
//       Entity* structure=wb.find(params[0]);
//       if(structure==NULL){
//         cerr << "Could not find the Assembly structure requested: " << params[0] << std::endl;
//         updated=false;
//       }
//       if(firstRequest && updated){
//         if(!ILAP::getInstance().updateWorldBaseWithAssemblyPlan(wb, structure)){
//           cerr << "Failed to understand Assembly plan" << endl;
//           updated=false;
//         }
//         firstRequest=false;
// //      }else{
// //        std::cout<<"Request a new plan to AP !"<<std::endl;
// //        //TODO ILAP::getInstance().error_on_part=ILGTP::getInstance().last_projected;
// //        if(!ILAP::getInstance().REPLAN_updateWorldBaseWithAssemblyPlan(wb)){
// //          cerr << "Failed to understand Assembly plan" << endl;
// //          updated=false;
// //          noMoreAPPlan=true;
// //        }
//       }
//       #endif //USE_ASSEMBLY_PLANNER
      
//       if(updated) {
//         VERBOSE_3 << "WorldBase state used by planning:" << endl << wb.toString() << endl;
//         vector<TaskListNode*> vecNodes;
//         vecNodes.push_back(new TaskListNode(TaskInstance(task, params, wb)));
//         TaskList TL(vecNodes);
//         PlanTree PT(TL);
//         timeval tim;
//         gettimeofday(&tim, NULL);
//         double t1=tim.tv_sec+(tim.tv_usec/1000000.0);
//         mainSessionPlanning(TL, PT, wb, LIMIT_TIME, MAX_PLANNING_TIME, stopAtFristPlan);
//         gettimeofday(&tim, NULL);
//         double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
//         ostringstream res;
//         res << "Planning process stopped after " << (t2 - t1)*1000 << " ms.";
//         //Planning time printed in the terminal by the core (hatponboard-lib/planning/hatpPlanningSession.cc)
//         mp.sendMessage(CONS_NAME, res.str());
//         Plan* plan = stock.getBestPlan();
//         if(plan) {
//           #ifdef USE_ASSEMBLY_PLANNER
//           planFound = true;
//           #endif //USE_ASSEMBLY_PLANNER
//           string res = makeMsgResult(plan, rqid);
//           mp.sendMessage(sender, res);
//           mp.sendMessage(CONS_NAME, res);
//         }
//         else if(stock.getSolutionExists()) {
//           string res = makeMsgAlready(rqid);
//           mp.sendMessage(sender, res);
//           mp.sendMessage(CONS_NAME, res);
//         }
//         else {
//           string res = makeMsgResult(NULL, rqid);
//           mp.sendMessage(sender, res);
//           mp.sendMessage(CONS_NAME, res);
//         }
//         while(!vecNodes.empty()){
//           delete vecNodes.back();
//           vecNodes.pop_back();
//         }
//       }
//       else {
//         mp.sendMessage(CONS_NAME, "Error(s) when updating facts database");
//         string res = makeMsgError(rqid);
//         mp.sendMessage(sender, res);
//         std::cerr << "ERROR: Impossible to update the facts database" << std::endl;
//       }
//     }
//     else {
//       if(htn_base.find(task).getNbArgs()>params.size()){
//         mp.sendMessage(CONS_NAME, "Error: The planning request is invalid. Not enough parameters given");
//         std::cerr << "ERROR: The planning request is invalid. Some parameters not found in world base or incorrect number of parameters" << std::endl;
//       }else if(htn_base.find(task).getNbArgs()<params.size()){
//         mp.sendMessage(CONS_NAME, "Error: The planning request is invalid. Too much parameters given");
//         std::cerr << "ERROR: The planning request is invalid. Some parameters not found in world base or incorrect number of parameters" << std::endl;
//       }else{
//         mp.sendMessage(CONS_NAME, "Error: The planning request is invalid. Some parameters not found in world base");
//         std::cerr << "ERROR: The planning request is invalid. Some parameters not found in world base or incorrect number of parameters" << std::endl;
//       }
//       string res = makeMsgError(rqid);
//       mp.sendMessage(sender, makeMsgError(rqid));
//     }
//     #ifdef USE_ASSEMBLY_PLANNER
//     }
//     #endif //USE_ASSEMBLY_PLANNER
//   }
//   else {
//     mp.sendMessage(CONS_NAME, "Error: The planning request is invalid. Task not found in HTN definition (task "+task+")");
//     std::cerr << "ERROR: The planning request is invalid. Task not found in HTN definition (task " << task << ")" << std::endl;
//     string res = makeMsgError(rqid);
//     mp.sendMessage(sender, makeMsgError(rqid));
//   }
// }

// void launchReplanning(string rqid, string planId, string taskId, string reason, vector<string> params, string sender) {
//   if(reason == RR_UNDEFINED) { // unknown ? what to do
//     // ...
//   }
//   else if(reason == RR_WCHANGE) { // world change
//     // ... complete replanning
//   }
//   else if(reason == RR_NEXTPLAN) { // asking for next plan
//     // ... return next plan 
//     Plan* plan = stock.getBestPlan();
//     if(plan) {
//       string res = makeMsgResult(plan, rqid);
//       mp.sendMessage(sender, res);
//       mp.sendMessage(CONS_NAME, res);
//     }
//     else if(stock.getSolutionExists()) {
//       string res = makeMsgAlready(rqid);
//       mp.sendMessage(sender, res);
//       mp.sendMessage(CONS_NAME, res);
//     }
//     else {
//       string res = makeMsgResult(NULL, rqid);
//       mp.sendMessage(sender, res);
//       mp.sendMessage(CONS_NAME, res);
//     }
//   }
//   else if(reason == RR_AGENTWORK) { // human does not want to work
//     // update effort_balancing...
//   }
//   else if(reason == RR_UNDSTATE) { //
//     // update undesirable state...
//   }
// }

// /** Alternative command line parameters parser.
//  *  This parser takes care of the detection of verbose level
//  */
// std::pair<std::string, std::string> verboseRule(const std::string& s){
//   if (s.find("-v") == 0) {
//     size_t count=0;
//     for(size_t i=0; i<s.size(); ++i){
//       size_t j=s.find("v", i);
//       if(j!=std::string::npos){
//         i=j;
//         ++count;
//       }
//       if(count>=3){
//         break;
//       }
//     }
//     std::stringstream stream;
//     stream<<count;
//     return std::make_pair(std::string("v"), stream.str());
//   } else {
//     return std::make_pair(std::string(), std::string());
//   }
// }

// void signalHandler(int s){
//   std::cerr<<"Caught signal \""<<strsignal(s)<<"\""<<std::endl;

//   if(mp.isConnected()){
//     mp.sendBroadcastMessage("{\"COMMAND\":\"EXIT\"}");
//   }
//   mp.disconnect();

//   exit(1);
// }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//=======================================================================================================
 hatpNode* FindNode(int ID, vector<hatpNode*> nodes){

   for (int i = 0; i < nodes.size(); ++i)
   {
     if(nodes[i]->getID() == ID){
       return nodes[i];
     }
   }

 }

//=======================================================================================================
Place* isPresent( hatpNode* action, vector<pair<Place*,unsigned int> > *add){
  vector<pair<Place*,unsigned int> > added = *add;
  for (int i = 0; i < added.size(); ++i)
  {
    //cout<<"isPresent:"<<added[i].second<< ","<<action->getID()<<endl;
    if(added[i].second == action->getID()){
      return added[i].first;
    }
  }
  //cout << "Sono nel null:"<<action->getID() << endl;
  return NULL;
}

//=======================================================================================================
void changePair(hatpNode* action, Place *p, vector<pair<Place*,unsigned int> > added){
  for (int i = 0; i < added.size(); ++i)
  {
    if(added[i].second == action->getID()){
      added[i].first = p;
    }
  }
}


//=======================================================================================================
void translate(PNPGenerator *pnpgen, Place *plast, hatpNode* curAction, vector<hatpNode*> nodes, vector<pair<Place*,unsigned int> > *added){

  bool stop = true;
  while(stop){
    vector< unsigned int> suc = curAction->getStreamSuccessors();
    if(suc.size()==0){
      //cout << "No successors"<< endl;
      stop = false;

      //To add the goal
      //----------------------------------------------
      Transition* t = pnpgen->pnp.addTransition("[]");
      Place* goal = pnpgen->pnp.addPlace("goal");
      Place *p0 =  plast;
      pnpgen->pnp.connect(p0,t);pnpgen->pnp.connect(t,goal);
      //----------------------------------------------

      return;
    }
    else if(suc.size()==1){
      hatpNode* nextAction = FindNode(suc[0], nodes);
      Place *p = isPresent(nextAction, added);
      if( p == NULL){
        string name = nextAction->getAgents()[0] + "#" + nextAction->getName();
        Transition *ts = pnpgen->pnp.addTransition(name+".start");
        Place *pe = pnpgen->pnp.addPlace(name+".exec");
        Transition *te = pnpgen->pnp.addTransition(name+".end");
        Place *pf = pnpgen->pnp.addPlace("X",-1);
        Place *p0 =  plast;
        ts->setY(p0->getY()); pe->setY(p0->getY()); // same line as p0
        te->setY(p0->getY()); pf->setY(p0->getY());
        ts->setX(p0->getX()+1);  pe->setX(p0->getX()+2); // X pos after p0
        te->setX(p0->getX()+3);  pf->setX(p0->getX()+4);
        pnpgen->pnp.connect(p0,ts); pnpgen->pnp.connect(ts,pe); pnpgen->pnp.connect(pe,te); pnpgen->pnp.connect(te,pf);
        pair<Place*, unsigned int> temp = make_pair(p0, nextAction->getID());
        added->push_back(temp);
        //cout<< name <<"_Id " <<  nextAction->getID() << endl;
        translate(pnpgen, pf, nextAction, nodes, added);
        //cout << "P" << p <<endl;
        return;
      }
      else{

        Node *ts = pnpgen->pnp.disconnect(p);
        Transition *tjoin = pnpgen->pnp.addTransition("tjoin");
        Place *p0new = pnpgen->pnp.addPlace("X",-1);
        pnpgen->pnp.connect(p,tjoin); pnpgen->pnp.connect(plast,tjoin); pnpgen->pnp.connect(tjoin, p0new); pnpgen->pnp.connect(p0new, ts);
        changePair(nextAction, p0new, *added);
        stop = false;

      }
    }
    else if(suc.size()>1){
      hatpNode* nextAction1 = FindNode(suc[0], nodes);
      hatpNode* nextAction2 = FindNode(suc[1], nodes);
      string name1 = nextAction1->getAgents()[0] + "#" + nextAction1->getName();
      string name2 = nextAction2->getAgents()[0] + "#" + nextAction2->getName();

      Transition *tfork = pnpgen->pnp.addTransition("tfork");
      Place *p01 = pnpgen->pnp.addPlace("X",-1);
      Place *p02 = pnpgen->pnp.addPlace("X",-1);

      Transition *ts1 = pnpgen->pnp.addTransition(name1+".start");
      Place *pe1 = pnpgen->pnp.addPlace(name1+".exec");
      Transition *te1 = pnpgen->pnp.addTransition(name1+".end");
      Place *pf1 = pnpgen->pnp.addPlace("X",-1);

      Transition *ts2 = pnpgen->pnp.addTransition(name2+".start");
      Place *pe2 = pnpgen->pnp.addPlace(name2+".exec");
      Transition *te2 = pnpgen->pnp.addTransition(name2+".end");
      Place *pf2 = pnpgen->pnp.addPlace("X",-1);

      pnpgen->pnp.connect(plast,tfork);pnpgen->pnp.connect(tfork,p01);pnpgen->pnp.connect(tfork,p02);
      pnpgen->pnp.connect(p01,ts1);pnpgen->pnp.connect(ts1,pe1);pnpgen->pnp.connect(pe1,te1);pnpgen->pnp.connect(te1,pf1);
      pnpgen->pnp.connect(p02,ts2);pnpgen->pnp.connect(ts2,pe2);pnpgen->pnp.connect(pe2,te2);pnpgen->pnp.connect(te2,pf2);

      pair<Place*, unsigned int> temp1 = make_pair(p01, nextAction1->getID());
      added->push_back(temp1);
      pair<Place*, unsigned int> temp2 = make_pair(p02, nextAction2->getID());
      added->push_back(temp2);


      translate(pnpgen, pf1, nextAction1, nodes, added);
      translate(pnpgen, pf2, nextAction2, nodes, added);
      return;
    }


  }

}

template <typename T> bool equalAttribute(T *a1, T *a2){
  return true;
}
//=======================================================================================================
//QUESTA E LA FUNZIONE CHE SEGUE SONO DA TRASFORMARE IN BOOLEAN VERSION
bool equalEntity(LightEntity *l1, LightEntity *l2){
  //Check if the entities have the same name
  std::string name1 = l1->getName();
  std::string name2 = l2->getName();
  if(l1->getName()!=l2->getName() || l1->getType()!=l2->getType()){
    cerr<<"The two entities compared have different name or are of different type!!!";
  }


  //Get all the attribute names
  std::list<std::string> Att1 = l1->getAllAttributeNames();
  std::list<std::string> Att2 = l2->getAllAttributeNames();

   
  if(Att1.size()==Att2.size()){ 
    //Divede the attributes in set of the same type
    LightAttributeSet<number> NumAtt1 = l1->getNumberAttrs();
    LightAttributeSet<std::string> StrAtt1 = l1->getStringAttrs();
    LightAttributeSet<bool> BoolAtt1 = l1->getBoolAttrs();
    AttributeSet<LightEntity*> EntAtt1 = l1->getEntityAttrs();

    LightAttributeSet<number> NumAtt2 = l2->getNumberAttrs();
    LightAttributeSet<std::string> StrAtt2 = l2->getStringAttrs();
    LightAttributeSet<bool> BoolAtt2 = l2->getBoolAttrs();
    AttributeSet<LightEntity*> EntAtt2 = l2->getEntityAttrs();

    std::list<std::string>::iterator iter1 = Att1.begin();
    std::list<std::string>::iterator iter2 = Att2.begin();
    //Create the vectors of attribite names of a certain type (number, string, bool and entity)
    std::vector<std::string> NameOfNumAtt;
    std::vector<std::string> NameOfStrAtt;
    std::vector<std::string> NameOfBoolAtt;
    std::vector<std::string> NameOfEntAtt;

    while(iter1!=Att1.end()){
    
    if(*iter1==*iter2){
      if(NumAtt1.hasAttributeOfName(*iter1) && NumAtt2.hasAttributeOfName(*iter2)){
        NameOfNumAtt.push_back(*iter1);
      }else if(StrAtt1.hasAttributeOfName(*iter1) && StrAtt2.hasAttributeOfName(*iter2)){
        NameOfStrAtt.push_back(*iter1);
      }else if(BoolAtt1.hasAttributeOfName(*iter1) && BoolAtt2.hasAttributeOfName(*iter2)){
        NameOfBoolAtt.push_back(*iter1);
      }else if(EntAtt1.hasAttributeOfName(*iter1) && EntAtt2.hasAttributeOfName(*iter2)){
        NameOfEntAtt.push_back(*iter1);
      }
      iter1++;
      iter2++;
    }else{
      cerr<<"Attributes with different names!!";
    }
    }

    //Check the number attributes
    if(!NameOfNumAtt.empty()){
      for (int i = 0; i < NameOfNumAtt.size() ; i++)
      {
        if(l1->get<number>(NameOfNumAtt[i])!=l2->get<number>(NameOfNumAtt[i])){
          return false;
        }
      }
    }

    //Check the string attributes
    if(!NameOfStrAtt.empty()){
      for (int i = 0; i < NameOfStrAtt.size() ; i++)
      {
        if(l1->get<string>(NameOfStrAtt[i])!=l2->get<string>(NameOfStrAtt[i])){
          return false;
        }
      }
    }

    //Check the boolean attributes
    if(!NameOfBoolAtt.empty()){
      for (int i = 0; i < NameOfBoolAtt.size() ; i++)
      {
        if(l1->get<bool>(NameOfBoolAtt[i])!=l2->get<bool>(NameOfBoolAtt[i])){
          return false;
        }
      }
    }

    //Check the Entity attributes DEVI RISOLVERE IL PROBLEMA DEL CASO IN CUI LA GET RESTITUISCE UN "NULL"
    if(!NameOfEntAtt.empty()){
      for (int i = 0; i < NameOfEntAtt.size() ; i++)
      {
        LightEntity* att1 = l1->get<LightEntity*>(NameOfEntAtt[i]);
        LightEntity* att2 = l2->get<LightEntity*>(NameOfEntAtt[i]);
        
        if (att1!=NULL && att2!=NULL)
        {
          if(att1->getName()!=att2->getName()){
            return false;
          }
        }else if((att1!=NULL && att2==NULL) || (att1==NULL && att2!=NULL)){
          return false;
        }
        
      }
    }

  }else{
    cerr<<"The two entities compared have different number of attributes!!!";
  }

  return true;
}

//=======================================================================================================
std::vector <pair<string, string> > getNameDiff(LightEntity *l1, LightEntity *l2){

  std::vector <pair<string, string> > result;
  pair<string, string> temp;
  //Check if the entities have the same name
  std::string name1 = l1->getName();
  std::string name2 = l2->getName();
  if(l1->getName()!=l2->getName() || l1->getType()!=l2->getType()){
    cerr<<"The two entities compared have different name or are of different type!!!";
  }

  //Get all the attribute names
  std::list<std::string> Att1 = l1->getAllAttributeNames();
  std::list<std::string> Att2 = l2->getAllAttributeNames();

   
  if(Att1.size()==Att2.size()){ 
    //Divede the attributes in set of the same type
    LightAttributeSet<number> NumAtt1 = l1->getNumberAttrs();
    LightAttributeSet<std::string> StrAtt1 = l1->getStringAttrs();
    LightAttributeSet<bool> BoolAtt1 = l1->getBoolAttrs();
    AttributeSet<LightEntity*> EntAtt1 = l1->getEntityAttrs();

    LightAttributeSet<number> NumAtt2 = l2->getNumberAttrs();
    LightAttributeSet<std::string> StrAtt2 = l2->getStringAttrs();
    LightAttributeSet<bool> BoolAtt2 = l2->getBoolAttrs();
    AttributeSet<LightEntity*> EntAtt2 = l2->getEntityAttrs();

    std::list<std::string>::iterator iter1 = Att1.begin();
    std::list<std::string>::iterator iter2 = Att2.begin();
    //Create the vectors of attribite names of a certain type (number, string, bool and entity)
    std::vector<std::string> NameOfNumAtt;
    std::vector<std::string> NameOfStrAtt;
    std::vector<std::string> NameOfBoolAtt;
    std::vector<std::string> NameOfEntAtt;

    while(iter1!=Att1.end()){
    
    if(*iter1==*iter2){
      if(NumAtt1.hasAttributeOfName(*iter1) && NumAtt2.hasAttributeOfName(*iter2)){
        NameOfNumAtt.push_back(*iter1);
      }else if(StrAtt1.hasAttributeOfName(*iter1) && StrAtt2.hasAttributeOfName(*iter2)){
        NameOfStrAtt.push_back(*iter1);
      }else if(BoolAtt1.hasAttributeOfName(*iter1) && BoolAtt2.hasAttributeOfName(*iter2)){
        NameOfBoolAtt.push_back(*iter1);
      }else if(EntAtt1.hasAttributeOfName(*iter1) && EntAtt2.hasAttributeOfName(*iter2)){
        NameOfEntAtt.push_back(*iter1);
      }
      iter1++;
      iter2++;
    }else{
      cerr<<"Attributes with different names!!";
    }
    }

    //Check the number attributes
    if(!NameOfNumAtt.empty()){
      for (int i = 0; i < NameOfNumAtt.size() ; i++)
      {
        if(l1->get<number>(NameOfNumAtt[i])!=l2->get<number>(NameOfNumAtt[i])){
          temp.first = l1->getName();
          temp.second = NameOfNumAtt[i];
          result.push_back(temp);
        }
      }
    }

    //Check the string attributes
    if(!NameOfStrAtt.empty()){
      for (int i = 0; i < NameOfStrAtt.size() ; i++)
      {
        if(l1->get<string>(NameOfStrAtt[i])!=l2->get<string>(NameOfStrAtt[i])){
          temp.first = l1->getName();
          temp.second = NameOfStrAtt[i];
          result.push_back(temp);
        }
      }
    }

    //Check the boolean attributes
    if(!NameOfBoolAtt.empty()){
      for (int i = 0; i < NameOfBoolAtt.size() ; i++)
      {
        if(l1->get<bool>(NameOfBoolAtt[i])!=l2->get<bool>(NameOfBoolAtt[i])){
          temp.first = l1->getName();
          temp.second = NameOfBoolAtt[i];
          result.push_back(temp);
        }
      }
    }

    //Check the Entity attributes DEVI RISOLVERE IL PROBLEMA DEL CASO IN CUI LA GET RESTITUISCE UN "NULL"
    if(!NameOfEntAtt.empty()){
      for (int i = 0; i < NameOfEntAtt.size() ; i++)
      {
        LightEntity* att1 = l1->get<LightEntity*>(NameOfEntAtt[i]);
        LightEntity* att2 = l2->get<LightEntity*>(NameOfEntAtt[i]);
        
        if (att1!=NULL && att2!=NULL)
        {
          if(att1->getName()!=att2->getName()){
            temp.first = l1->getName();
            temp.second = NameOfEntAtt[i];
            result.push_back(temp);
          }
        }else if((att1!=NULL && att2==NULL) || (att1==NULL && att2!=NULL)){
          temp.first = l1->getName();
          temp.second = NameOfEntAtt[i];
          result.push_back(temp);
        }
        
      }
    }

  }else{
    cerr<<"The two entities compared have different number of attributes!!!";
  }

  return result;
}

//=======================================================================================================
//This function check is two world bases are equal.
bool areEqualWB( LightWorldBase *lw1, LightWorldBase *lw2){

  //Get the databases of the two LightWorldBase states
  std::map<std::string, LightEntity*> data1;
  std::map<std::string, LightEntity*> data2;
  data1 = lw1->getDatabase();
  data2 = lw2->getDatabase();
  
  //If the size of the two database are different the database are different 
  if(data1.size()!=data2.size())
  {
    return false;
  //If the size of the two database are equal check if they are equal
  }else
  { 
    //Iterator on the first database
    for(std::map<std::string, LightEntity*>::iterator it=data1.begin();  it!= data1.end(); it++){
    LightEntity* l1 = it->second;
    LightEntity* l2 = lw2->find(it->first);
    if(equalEntity(l1,l2)==false){
      return false;
    }
   }
  }

  return true;

}
//=======================================================================================================
//This function check if a vector of world bases are equal.
bool areAllEqualWBs(std::vector<LightWorldBase*> WorldBases){

for (int i = 1; i < WorldBases.size(); i++)
{
  if(!areEqualWB(WorldBases[0],WorldBases[i])){
    return false;
  }
}
return true;
}

//=======================================================================================================

std::vector<std::vector<pair<LightWorldBase*, int > > > GroupByBoolVar2(std::vector<pair<LightWorldBase*, int> > WorldBases, pair<string, string> EntAtt){
  
  std::vector<std::vector<pair<LightWorldBase*, int> > > result;
  std::vector<std::vector<int> > result1;

  std::vector<pair<LightWorldBase*,int> > trueWB;
  std::vector<pair<LightWorldBase*,int> > falseWB;

  std::vector<int> indTrueWB;
  std::vector<int> indFalseWB;
  
  LightEntity* temp;
  bool val;

  for (int i = 0; i < WorldBases.size(); i++)
  {
    temp = WorldBases[i].first->find(EntAtt.first);
    //INSERISCI UN ERROR NEL CASO IN CUI L'ATTRIBTO NON è UN BOOL
    if (temp->get<bool>(EntAtt.second)==true)
    {
      trueWB.push_back(WorldBases[i]);
      indTrueWB.push_back(i);
    }else
    {
      falseWB.push_back(WorldBases[i]);
      indFalseWB.push_back(i);
    }
  }
  
  result.push_back(trueWB); result.push_back(falseWB);
  result1.push_back(indTrueWB); result1.push_back(indFalseWB);
  
  //result.push_back(std::vector<LightWorldBase*>());


  return result;
  

}

//=======================================================================================================
std::vector <pair<string, string> > deleteDuplicate(std::vector <pair<string, string> > vector){

  std::vector <pair<string, string> > result;
  bool present = false;

  for (int i = 0; i < vector.size() ; i++)
  {
    for (int j = 0; j < result.size(); j++)
    {
      if (vector[i].first == result[j].first && vector[i].second == result[j].second )
      {
        present = true;
      }
    }
      if(!present)
      {
        result.push_back(vector[i]);
      }
      present = false;
  }
  return result;

}
//=======================================================================================================

//Return the names of the parameters that are differents
//Maybe it is better to change this function such as to consider only difference of boolean variable.
std::vector <pair<string, string> >  getDiff(std::vector<LightWorldBase*> WorldBases){

  std::vector <pair<string, string> > result;

  for (int i = 0; i < WorldBases.size(); i++)
  {
    for (int j = i+1; j < WorldBases.size(); j++)
    {
      
     //Get the databases of the two LightWorldBase states
     std::map<std::string, LightEntity*> data1;
     std::map<std::string, LightEntity*> data2;
     data1 = WorldBases[i]->getDatabase();
     data2 = WorldBases[j]->getDatabase();

    //Iterator on the first database
    for(std::map<std::string, LightEntity*>::iterator it=data1.begin();  it!= data1.end(); it++){
    LightEntity* l1 = it->second;
    LightEntity* l2 = WorldBases[j]->find(it->first);

    std::vector<pair<string, string> > temp = getNameDiff(l1,l2);

    for (int k = 0; k < temp.size(); k++)
    {
      result.push_back(temp[k]);
    }
   }

    }
  }
  
  result = deleteDuplicate(result);
  //cout<<"SIZE:"<<result.size()<<endl;
  for (int i = 0; i < result.size(); i++)
  {
    //cout<<"Var:"<<result[i].first<<"."<<result[i].second<<endl;
  }

  return result;

}

//=======================================================================================================
pair< string, string> getAgentAction(string ActionName){

  pair<string, string > result;
  if(ActionName!="tfork"){
    result.first = ActionName.substr(0,ActionName.find_first_of("#"));
    result.second = ActionName.substr(ActionName.find_first_of("#")+1, ActionName.find_last_of(".")-ActionName.find_first_of("#")-1);
  }else{
    result.first = "noun";
    result.second = "tfork";
  }
  return result;

}

bool checkCor(std::vector<pair<string, string> > AgentAndAction){
  //cout<<AgentAndAction.size()<<endl;
  string temp = AgentAndAction[0].second;
  if (temp=="tfork")
  {
    for (int i = 1; i < AgentAndAction.size(); i++)
    {
      if (AgentAndAction[i].second !="tfork")
      {
        return false;
      }
    }
  }else if (temp!="tfork")
  {
    for (int i = 1; i < AgentAndAction.size(); i++)
    {
      if (AgentAndAction[i].second=="tfork")
      {
        return false;
      }
    }
  }
  return true;
}

//=======================================================================================================
LightWorldBase updateWorldState(LightWorldBase WB, pair<string, string> AgentAndAction, hatpPlan HATPplan){

  std::vector< hatpNode * > actionNodes = HATPplan.getActionNodes();
  hatpNode * curActionNode;
  for (int i = 0; i < actionNodes.size(); i++)
  {
    if(actionNodes[i]->getName() == AgentAndAction.second){
      curActionNode = actionNodes[i];
    }
  }
  
  std::vector<std::string> args;
  args = curActionNode->getParameters();
  TaskInstance CurTaskIns (curActionNode->getName(), args, WB);
  CurTaskIns.applyActionEffects(WB);

  return WB;

}

//=======================================================================================================
LightWorldBase updateWorldState2(LightWorldBase WB, std::vector<string> parActions, hatpPlan HATPplan){
  std::vector<hatpNode*> actionNodes = HATPplan.getActionNodes();
  hatpNode* curActionNode;
  string temp;
  for (int i = 0; i < parActions.size(); i++)
  {
    temp = parActions[i];
    //cout<<temp<<endl;
    temp = temp.substr(temp.find_first_of("#")+1, temp.find_last_of(".")-temp.find_first_of("#")-1);
    //cout<<temp<<endl;
    for (int j = 0; j < actionNodes.size(); j++)
    {
      if (actionNodes[j]->getName() == temp)
      {
        curActionNode = actionNodes[j];
      }
    }
    std::vector<std::string> args = curActionNode->getParameters();
    TaskInstance CurTaskIns(curActionNode->getName(), args, WB);
    CurTaskIns.applyActionEffects(WB);
  }
return WB;
}

//=======================================================================================================
std::vector <Node*> addSensActions(PNPGenerator* mergePnp, Node* genCurNode, std::vector<pair<string, string>> Var){
  
  std::vector<Node*> curBranches;
  curBranches.push_back(genCurNode);
  std::vector <Node*> temp;

  for (int i = 0; i < Var.size(); i++)
  {
    for (int j = 0; j < curBranches.size(); j++)
    {
      Transition* T1 = mergePnp->pnp.addTransition("["+Var[i].second + "]");
      Transition* T2 = mergePnp->pnp.addTransition("[not "+Var[i].second + "]");
      Place* P1 = mergePnp->pnp.addPlace("X",-1);
      Place* P2 = mergePnp->pnp.addPlace("X",-1);
      mergePnp->pnp.connect(curBranches[j], T1);mergePnp->pnp.connect(curBranches[j], T2);mergePnp->pnp.connect(T1, P1);mergePnp->pnp.connect(T2, P2);
      temp.push_back(P1); temp.push_back(P2);
    }
    curBranches = temp;
    temp.clear();
  }
  return curBranches;
}
//=======================================================================================================

 //This function is used to overcome the problem of fake fork. If the node it is not a fork the fuction returns
 //the node, if it is a real fork it return the current node and if it is a fake fork return the first action on
 //the non fake branch.
 Node* noFakeFork(PNPGenerator pnp, Node* node){
  Node* n;
  if(node->getName()!="tfork"){
    return node;
  }else{
  std::vector<Node*> nextFork = pnp.pnp.nextAll(node);
  Node* n1 = nextFork[0]; n1 = pnp.pnp.next(n1);
  Node* n2 = nextFork[1]; n2 = pnp.pnp.next(n2);
  if(n1->getName()=="tjoin"){
    return n2;
  }else if(n2->getName()=="tjoin"){
    return n1;
  }else{
    return node;
  }
  }
}

void mergePnpPlans(PNPGenerator* mergePnp, Node* genCurNode, std::vector<PNPGenerator> PNPplans, std::vector<hatpPlan*> HATPplans, std::vector<LightWorldBase> WorldStates, std::vector<Node*> curNodes){


//cout<<"Sono qui!!"<<endl;
std::vector<string> curActionsName;
std::vector<Node*> nextNodes;

std::vector<pair<string, string> > AgentAndAction;

//Flag to point if the actions considered at the iteration i are a group of parallel actions, or if they are a group of single action
bool singleAction;
std::list<pair<int, std::vector<string> > > ParallelActions;

//Select the next nodes to add at the merge plan and compute the relative world states
for (int i = 0; i < PNPplans.size(); ++i)
{
  //Select the right current nodes
  if(curNodes[i]->getName() == "init"){
    curNodes[i] = PNPplans[i].pnp.next(curNodes[i]);
  }else{
    curNodes[i] = PNPplans[i].pnp.next(curNodes[i]);//Da migliorare crea una funzione next action.
    curNodes[i] = PNPplans[i].pnp.next(curNodes[i]);
    curNodes[i] = PNPplans[i].pnp.next(curNodes[i]);
    nextNodes = PNPplans[i].pnp.nextAll(curNodes[i]);

     if (nextNodes[0]->getName()!="[]")
     {
       curNodes[i] = nextNodes[0];
     }else if (nextNodes[0]->getName()=="[]")
     {
       Transition *t = mergePnp->pnp.addTransition("[]");
       Place* goal = mergePnp->pnp.addPlace("goal");
       mergePnp->pnp.connect(genCurNode,t);mergePnp->pnp.connect(t,goal);
       HATPplans.erase(HATPplans.begin()+i);
       WorldStates.erase(WorldStates.begin()+i);
       curNodes.erase(curNodes.begin()+i);
       PNPplans.erase(PNPplans.begin()+i);
       break;
    
    }

    
  }

  //curActionsName.push_back(curNodes[i]->getName()); 
  //AgentAndAction.push_back(getAgentAction(curActionsName[i]));
  
   //Check if the current node is a fake fork. Fake fork are fork in which one 
   //branch is fake and can be produced in HATP by superflous causal link.
   curNodes[i] = noFakeFork(PNPplans[i], curNodes[i]);
   string actionName = curNodes[i]->getName();
   //cout<<actionName<<endl;
   //If the algorithm finds a tjoin at this point it means that it cames from a fake fork.
   //For this reason the tjoint is not considered and curNode becames the next .start
   if(actionName=="tjoin"){
        curNodes[i] = PNPplans[i].pnp.next(curNodes[i]);
        curNodes[i] = PNPplans[i].pnp.next(curNodes[i]);
        actionName = curNodes[i]->getName();
        curActionsName.push_back(actionName); 
    }else{
      curActionsName.push_back(actionName); 
    }
   AgentAndAction.push_back(getAgentAction(curActionsName[i]));

  
  if (curActionsName[i]!="tfork" )
  {
    //Set the singleAction flag equal to true
    singleAction = true;
       
    //Compute the world base after the current action
    WorldStates[i] = updateWorldState(WorldStates[i],AgentAndAction[i], *HATPplans[i]);
    
  }else{
    //Siamo in presenza di un nodo fork. Si crea una struttura contenente tutte le azioni eseguite parallelamente.
    //Set the singleAction flag equal to false
    singleAction = false;

    Node* temp;
    std::vector<Node*> nextTemp2;
    //The list of the group of parallel actions associated to the id of the plan
    std::vector<Node*> nextFork = PNPplans[i].pnp.nextAll(curNodes[i]);

    //Group of actions for the plan i 
    std::pair<int,std::vector<string> > group_i; 
    group_i.first = i;
    group_i.second = std::vector<string>();

    for (int k = 0; k < nextFork.size(); k++)
    {
      temp = nextFork[k];

      while(temp->getName()!="tjoin")
      {
        temp = PNPplans[i].pnp.next(temp);
        if(temp->getName()!="[]"){
        group_i.second.push_back(temp->getName());
        }
        temp = PNPplans[i].pnp.next(temp);
        temp = PNPplans[i].pnp.next(temp);
        temp = PNPplans[i].pnp.next(temp);
        nextTemp2 = PNPplans[i].pnp.nextAll(temp);
        //Condizione di uscita nel caso i piani finiscano con il parallelismo
        if(nextTemp2.size()!=0){
         temp = nextTemp2[0];
        }else{
         break;
        }
      }
    }
    //Condizione di uscita nel caso i piani finiscano con il parallelismo
    if(nextTemp2.size()!=0){
    temp = PNPplans[i].pnp.next(temp);
    temp = PNPplans[i].pnp.next(temp); 
    group_i.second.push_back(temp->getName());
    }
    //Apdate the world state after the execution of the parallel actions
    WorldStates[i] = updateWorldState2(WorldStates[i], group_i.second, *HATPplans[i]);
    
  }

}

//If there are not more plans to merge return
if(PNPplans.size()==0){
  return;
}

if (!checkCor(AgentAndAction))
 {
   cerr<<"ERROR: Trying to merge a single action with parallel actions."<<endl;
 }

//Vettore dei puntatori ai world bases
std::vector<LightWorldBase*> WorldStatesAddr;
for (int i = 0; i < WorldStates.size(); i++)
{
  WorldStatesAddr.push_back(&WorldStates[i]);
}

if(singleAction){

  if(areAllEqualWBs(WorldStatesAddr))
  {
    //aggiungi un solo nodo e condizione stesso Agente

    //Nome del nodo da aggiungere
    string name = AgentAndAction[0].first + "#" + AgentAndAction[0].second;
    //Add the action to the merge plan
    Transition *ts = mergePnp->pnp.addTransition(name+".start");
    Place *pe = mergePnp->pnp.addPlace(name+".exec");
    Transition *te = mergePnp->pnp.addTransition(name+".end");
    Place *pf = mergePnp->pnp.addPlace("X",-1);
    //Update the action counter
    numberOfActions = numberOfActions +1;
  //Connect the new node to the merge plan.
    mergePnp->pnp.connect(genCurNode,ts); mergePnp->pnp.connect(ts,pe); mergePnp->pnp.connect(pe,te);  mergePnp->pnp.connect(te,pf); 
   
   //Continue the merging 
   mergePnpPlans(mergePnp, pf, PNPplans, HATPplans, WorldStates, curNodes);
   //cout<<"Sono Qui"<<endl;

  }
  //else if (!areAllEqualWBs(WorldStatesAddr) && doneByTheSame(AgentAndAction))
  else if (!areAllEqualWBs(WorldStatesAddr))
  {
    //cout<<"Sono Qui2"<<endl;
    //Da cambiare in difAttr
    std::vector <pair<string, string> > differentVarValue = getDiff(WorldStatesAddr);
  
    
    //--------------------------------------------------------
    std::vector<std::vector<pair<LightWorldBase*,int> > >  WBaseByVar;
    std::vector<std::vector<int> > indWBaseByVar;
    std::vector<std::vector<pair<LightWorldBase*, int> > > Temp3;
    std::vector<std::vector<pair<LightWorldBase*, int> > > Temp4;

    //To group the WB 
    std::vector<pair<LightWorldBase*, int> > WbId;
    pair<LightWorldBase*, int> tempPair;
    for (int i = 0; i < WorldStatesAddr.size(); i++)
    {
      tempPair.first = WorldStatesAddr[i];
      tempPair.second = i;
      WbId.push_back(tempPair);
    }
    WBaseByVar = GroupByBoolVar2( WbId, differentVarValue[0]);
    //--------------------------------------------------------
   

    for (int i = 1; i < differentVarValue.size(); ++i)
    {
      for (int j = 0; j < WBaseByVar.size(); j++)
      {
        Temp3 = GroupByBoolVar2( WBaseByVar[j], differentVarValue[i]);
        Temp4.push_back(Temp3[0]); Temp4.push_back(Temp3[1]);
      }


      WBaseByVar = Temp4;
      for (int k = 0; k < Temp4.size(); k++)
      {
        Temp4.pop_back(); Temp4.pop_back();
      }
    }
   
    //-----------------------------------------------------------------
    for (int i = 0; i < WBaseByVar.size(); i++)
    {
      indWBaseByVar.push_back(std::vector<int>());
      for (int j = 0; j < WBaseByVar[i].size(); j++)
      {
        indWBaseByVar[i].push_back(WBaseByVar[i][j].second);
      }
    }
    //----------------------------------------------------------------- 

   std::vector<std::vector< PNPGenerator> > grPNPplans;
   std::vector<std::vector<hatpPlan*> > grHATPplans;
   std::vector<std::vector<LightWorldBase> > grWorldStates;
   std::vector<std::vector<pair<string, string> > > grAgentAndAction;
   std::vector<std::vector<Node*> > grcurNodes;

    for (int i = 0; i < indWBaseByVar.size(); i++)
    {
     grPNPplans.push_back(std::vector< PNPGenerator>());
     grHATPplans.push_back(std::vector<hatpPlan*>());
     grWorldStates.push_back(std::vector<LightWorldBase>());
     grAgentAndAction.push_back(std::vector<pair<string, string> >());
     grcurNodes.push_back(std::vector<Node*>());
     for (int j = 0; j < indWBaseByVar[i].size(); j++)
     {
       grPNPplans[i].push_back(PNPplans[indWBaseByVar[i][j]]);
       grHATPplans[i].push_back(HATPplans[indWBaseByVar[i][j]]);
       grWorldStates[i].push_back(WorldStates[indWBaseByVar[i][j]]);
       grAgentAndAction[i].push_back(AgentAndAction[indWBaseByVar[i][j]]);
       grcurNodes[i].push_back(curNodes[indWBaseByVar[i][j]]);
     }
    }


    std::size_t found = AgentAndAction[0].second.find("_");
    string name;
    if (found!=std::string::npos){
    string nac = AgentAndAction[0].second.substr(0,AgentAndAction[0].second.find_first_of("_"));
      name = AgentAndAction[0].first + "#" + nac;
    }else{
      name = "Human#Answer";
    }
    //Add the current action (remenber to set the correct name)
    Transition* ts = mergePnp->pnp.addTransition(name+".start");
    Place* pe = mergePnp->pnp.addPlace(name+".exec");
    Transition* te = mergePnp->pnp.addTransition(name+".end");
    Node* pf =mergePnp->pnp.addPlace("X",-1);
    //Update the action counter
    numberOfActions = numberOfActions +1;
    mergePnp->pnp.connect(genCurNode,ts);mergePnp->pnp.connect(ts, pe);mergePnp->pnp.connect(pe,te);mergePnp->pnp.connect(te,pf);
    //Add the sensing action
    std::vector<Node*> branches = addSensActions(mergePnp, pf, differentVarValue);


    for (int i = 0; i < branches.size(); i++)
    {
      //Call the mergePnpPlans for each group of plans
      mergePnpPlans(mergePnp, branches[i], grPNPplans[i], grHATPplans[i], grWorldStates[i], grcurNodes[i]);
    }

  }

}else{
  //Parallel actions case
  //Case:1, same actions done by the same agents
  if (areAllEqualWBs(WorldStatesAddr))
  {
    Transition* tfork = mergePnp->pnp.addTransition("tfork");
    mergePnp->pnp.connect(genCurNode,tfork);
    Transition* tjoin = mergePnp->pnp.addTransition("tjoin");
    std::vector<Node*> nextFork = PNPplans[0].pnp.nextAll(curNodes[0]);
    Node* temp;
    std::vector<Node*> nextTemp2;
    string actName;
    Place* p;
    Place* pf;

    for (int i = 0; i < nextFork.size(); i++)
    {
      p =mergePnp->pnp.addPlace("X",-1);
      mergePnp->pnp.connect(tfork,p);
      temp = nextFork[i];
      temp = PNPplans[0].pnp.next(temp);
      actName = temp->getName();

      while(actName!="tjoin")
      {
        if(actName!="[]"){
          actName = actName.substr(0,actName.find_last_of("."));
          Transition* ts = mergePnp->pnp.addTransition(actName + ".start");
          Place* pe = mergePnp->pnp.addPlace(actName + ".exec");
          Transition* te = mergePnp->pnp.addTransition(actName + ".end");
          pf =mergePnp->pnp.addPlace("X",-1);

          //Update the action counter
          numberOfActions = numberOfActions +1;

          mergePnp->pnp.connect(p,ts);mergePnp->pnp.connect(ts,pe);mergePnp->pnp.connect(pe,te);mergePnp->pnp.connect(te,pf);
          p = pf;
          temp = PNPplans[0].pnp.next(temp);temp = PNPplans[0].pnp.next(temp);temp = PNPplans[0].pnp.next(temp);
          nextTemp2 = PNPplans[0].pnp.nextAll(temp);
        }else{
          temp = PNPplans[0].pnp.next(temp);
          nextTemp2 = PNPplans[0].pnp.nextAll(temp);
        }
        //Condizione di uscita nel caso i piani finiscano con il parallelismo
        if (nextTemp2.size()!=0)
        {
          temp = nextTemp2[0];
        }else{
          break;
        }
        actName = temp->getName();
      } 
      mergePnp->pnp.connect(pf,tjoin);
    }
    Place* pp;
    //Condizione di uscita nel caso i piani finiscano con il parallelismo
    if(nextTemp2.size()!=0){
    //Add the action after the join 
    temp = PNPplans[0].pnp.next(temp);temp = PNPplans[0].pnp.next(temp);
    actName = temp->getName();
    actName = actName.substr(0,actName.find_last_of("."));

    Place* ps = mergePnp->pnp.addPlace("X",-1);
    Transition* ts = mergePnp->pnp.addTransition(actName + ".start");
    Place* pe = mergePnp->pnp.addPlace(actName + ".exec");
    Transition* te = mergePnp->pnp.addTransition(actName + ".end");
    pp = mergePnp->pnp.addPlace("X",-1);

    //Update the action counter
    numberOfActions = numberOfActions +1;

    mergePnp->pnp.connect(tjoin,ps);mergePnp->pnp.connect(ps,ts);mergePnp->pnp.connect(ts,pe);mergePnp->pnp.connect(pe,te);mergePnp->pnp.connect(te,pp);
    }else{
      //VEDI SE RITORNARE CHIAMANDO IL MERGE SUI VETTORI VUOTI FORSE È MEGLIO PER COERENZA DI CODICE
      pp = mergePnp->pnp.addPlace("X",-1);
      mergePnp->pnp.connect(tjoin,pp);
      Transition* t = mergePnp->pnp.addTransition("[]");
      Place* goal = mergePnp->pnp.addPlace("goal");
      mergePnp->pnp.connect(pp,t);
      mergePnp->pnp.connect(t,goal);
      return;
    }
    //Apdate the current nodes that have to be passed in the next mergePnpPlans iteration 
    Node* curTemp;
    for (int i = 0; i < curNodes.size(); i++)
    {
      Node* t = PNPplans[i].pnp.next(curNodes[i]);
      t = PNPplans[i].pnp.next(t);
      while(t->getName() != "tjoin")
      {
        t = PNPplans[i].pnp.next(t);
        curTemp = t;
        t = PNPplans[i].pnp.next(t);
        t = PNPplans[i].pnp.next(t);
        nextTemp2 = PNPplans[i].pnp.nextAll(t);
        //Condizione di uscita nel caso i piani finiscano con il parallelismo
        if(nextTemp2.size()!=0){
          t = nextTemp2[0];
        }else{
          break;
        }

      }
      //Condizione di uscita nel caso i piani finiscano con il parallelismo
      if(nextTemp2.size()!=0){
       t = PNPplans[i].pnp.next(t);t = PNPplans[i].pnp.next(t);
       curNodes[i] = t;
      }else{
        curNodes[i] = curTemp;
      }
    }

    //Iterate the merging proces on the next actions of the PNPplans
    mergePnpPlans(mergePnp, pp, PNPplans, HATPplans, WorldStates, curNodes);

}
// else if (!areAllEqualWBs(WorldStatesAddr) && doneByTheSame(ParallelActions))
  else if (!areAllEqualWBs(WorldStatesAddr))
  {
    cerr<<"ERROR: Group of parallel actions can not produce differente world state. Decisions have to be taken before parallel actions. Check the domain."<<endl;
  }

}

return;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*
 * ========================================================
 *          MAIN METHOD
 * ========================================================
 */

int main(int argc, char *argv[]){

std::vector<string> PlanRequests;
string line;

if (argc<2) {
        cout << "    Use: " << argv[0] << " <PlanRequestFile> " << endl;
        cout << "Example: " << argv[0] << " BoxTwoItemsAndAsk.txt " << endl;
        return -1;
    }

ifstream myfile (argv[1]);
  if (myfile.is_open())
  {
    while ( getline (myfile,line) )
    {
      PlanRequests.push_back(line);
    }
    myfile.close();
  }else{
   cout << "Unable to open file"; 
   return 0;
  }

int type = 1;


 std::vector<hatpPlan*> HATPplans;
 //Vector of names pnp plan files generated
 std::vector<std::string> namePlanFiles;
 //Vector of the pnpgenerator created by the proces
 std::vector<PNPGenerator> PlanGenerated;
 
 //Initialize the msgClient
 msgClient* mp = new msgClient();
 mp->connect("client", "localhost", 5500);
 //If msgClient is connected send the plan requests
 if(mp->isConnected()) {
 
 

 //For each plan request in PlanRequests send a request to HATP and transform the HATP output in a PNP plan
 for (int j = 0; j < PlanRequests.size(); ++j){ 
    mp->sendMessage("HATP", PlanRequests[j]);
    pair<string, string> result = mp->getBlockingMessage();
    
    //To plot the plan obtained
    //cout << "Plan obtained: " << result.second << endl;

    string toAnalyse(result.second);
    removeFormatting(toAnalyse);
    hatpPlan* myPlan = new hatpPlan(toAnalyse);
    HATPplans.push_back(myPlan);

    if(myPlan->isPlanValid()){
      vector<hatpNode*> nodes = myPlan->getActionNodes();
      hatpStreams* planStreams = myPlan->getStreams();
      vector<hatpNode*> firstActions;

      //Initialize the PNPGenerator
      PNPGenerator pnpgen("Plan" + boost::lexical_cast<std::string>(j));
      PNPGenerator *pnpg = &pnpgen;
      namePlanFiles.push_back(pnpgen.pnp.getName());
      

      //Initialize the vector added. This vector is a collection of the action added to the PNP.
      //It avoids to add action previously added.
      vector<pair<Place* , unsigned int> > added;
  
      //Find the first actions  
      for (int i = 0; i < nodes.size(); ++i)
        {
        if(nodes[i]->getStreamPredecessors().size() == 0){
          firstActions.push_back(nodes[i]);
        }
      }
  
      //Constract the PNP plan from the HATP plan
      if (firstActions.size() == 1)
      {
        string name = firstActions[0]->getAgents()[0] + "#" + firstActions[0]->getName(); 
        Transition *ts = pnpgen.pnp.addTransition(name+".start");
        Place *pe = pnpgen.pnp.addPlace(name+".exec");
        Transition *te = pnpgen.pnp.addTransition(name+".end");
        Place *pf = pnpgen.pnp.addPlace("X",-1);
        Place *p0 =  pnpgen.pnp.pinit;
        ts->setY(p0->getY()); pe->setY(p0->getY()); // same line as p0
        te->setY(p0->getY()); pf->setY(p0->getY());
        ts->setX(p0->getX()+1);  pe->setX(p0->getX()+2); // X pos after p0
        te->setX(p0->getX()+3);  pf->setX(p0->getX()+4);
        pnpgen.pnp.connect(p0,ts); pnpgen.pnp.connect(ts,pe); pnpgen.pnp.connect(pe,te); pnpgen.pnp.connect(te,pf);
        pair<Place*, unsigned int> temp = make_pair(p0,firstActions[0]->getID());
        added.push_back(temp);
        translate(pnpg, pf, firstActions[0], nodes, &added);
      }else{
        Transition *tfork = pnpgen.pnp.addTransition("tfork");
        Place *p0 =  pnpgen.pnp.pinit;
        pnpgen.pnp.connect(p0,tfork);
        for (int i = 0; i < firstActions.size(); ++i)
          {
          string name = firstActions[i]->getAgents()[0] + "#" + firstActions[i]->getName(); 
          Place* ps =pnpgen.pnp.addPlace("X",-1);;
          Transition *ts = pnpgen.pnp.addTransition(name+".start");
          Place *pe = pnpgen.pnp.addPlace(name+".exec");
          Transition *te = pnpgen.pnp.addTransition(name+".end");
          Place *pf = pnpgen.pnp.addPlace("X",-1);
          Place *p0 =  pnpgen.pnp.pinit;
          ts->setY(p0->getY()); pe->setY(p0->getY()); // same line as p0
          te->setY(p0->getY()); pf->setY(p0->getY());
          ts->setX(p0->getX()+1);  pe->setX(p0->getX()+2); // X pos after p0
          te->setX(p0->getX()+3);  pf->setX(p0->getX()+4);
          pnpgen.pnp.connect(tfork,ps);pnpgen.pnp.connect(ps,ts); pnpgen.pnp.connect(ts,pe); pnpgen.pnp.connect(pe,te); pnpgen.pnp.connect(te,pf);
          pair<Place*, unsigned int> temp = make_pair(p0,firstActions[i]->getID());
          added.push_back(temp);
          translate(pnpg, pf, firstActions[i], nodes, &added);
        }
      }
     //Save the PNP plan(spostato)
     PlanGenerated.push_back(pnpgen);
     pnpgen.save();

    }else{

      return 0;
    }

  }

 }

 //Initialize the vector of wolrd state vector. Each vector represents the set of the wolrd state after each action.
 //At the beginning we consider only HATP plan composed by a single stream. Then we will generalize 
 std::vector<std::vector<LightWorldBase> > WorldStates;
 std::vector<LightWorldBase> InitialWorldStates;
 std::vector<LightWorldBase> InitWB;

 //The start actions of the current plan. To note that this approach is god only for plans with only one start action.
  //hatpNode* StartActions;
  std::vector<std::vector<hatpNode*> > StartActions;
 // //The following code is god only in the case in wich the diffent plans start with the same world state.
 // //To change in the case of same plan request starting from different initial wolrd state. In particular
 // //the following world base initialization has to be done after finding each HATP plan.


  initWorldBase(wb_init);
  initHTNBase(htn_base);
  TaskInstance::htn_base = &htn_base;
  TaskListNode::count = START_COUNT_ID;  


wb_light = LightWorldBase(wb_init);
PNPGenerator pnpmerg("PlanMerging");
PNPGenerator *pnpg = &pnpmerg;
Node* genInitNode = pnpmerg.pnp.pinit;
std::vector<Node*> initNodes;
 for (int i = 0; i < PlanGenerated.size(); i++)
 {
   initNodes.push_back(PlanGenerated[i].pnp.pinit);
   InitWB.push_back(wb_light);
 }


mergePnpPlans(pnpg, genInitNode, PlanGenerated, HATPplans, InitWB, initNodes);
pnpmerg.save();
cout << "Number Of Actions:"<<numberOfActions<<endl;
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

}

