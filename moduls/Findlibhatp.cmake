# Find the libhatp module (this file is meant to be installed using make install))
# Once found the following variables are set :
#	libhatp_FOUND	True to specify that the module has been found
#
#	libhatp_INCLUDE_DIRS	Path(s) to the headers
#
#	libhatp_LIBRARIES	Link these to use libhatp

# To help CMake in finding the libhatp module
# one can set the following variables :
# libhatp_DIR	Those variables can be either set using 'set' command of
# or			CMake, or using '-D<var>=value' while calling CMake or 
# libhatp_ROOT	finally they can be set as environment variables.
# or			Those three variables are aliases, so just set one is enough.
# libhatpROOT	
#
# You can set any of them using one of the following methods :
#	1) Set it as environnement varialbe (e.g. export)
#	2) Give it as argument for cmake : cmake -Dlibhatp_DIR="...."
#	3) In a CMakeLists.txt, set a variable : set(libhatp_DIR ...)

#Varaibles to change if the module changes
set(HEADER_FILES hatpTaskList.hh;hatpLexer.hh;hatpNode.hh;hatpPlan.hh;hatpTree.hh;hatpStreams.hh;hatpTools.hh)
set(INCLUDE_PATH_SUFFIXES include include/libhatp libhatp)
set(LIBRARIES libhatp)
set(CURRENT_PREFIX /opt/openrobots)

foreach(header ${HEADER_FILES})
	find_path(${header}_PATH ${header} PATH_SUFFIXES ${INCLUDE_PATH_SUFFIXES}
	HINTS ${CURRENT_PREFIX}
	${libhatp_DIR} ${libhatp_ROOT} ${libhatpROOT}
	$ENV{libhatp_DIR} $ENV{libhatp_ROOT} $ENV{libhatpROOT})
	if(${header}_PATH)
		list(APPEND libhatp_INCLUDE_DIRS ${${header}_PATH})
		unset(${header}_PATH CACHE)
	else(${header}_PATH)
		list(APPEND MISSING_HEADERS ${header})
	endif(${header}_PATH)
endforeach()

if(libhatp_INCLUDE_DIRS)
	list(REMOVE_DUPLICATES libhatp_INCLUDE_DIRS)
endif(libhatp_INCLUDE_DIRS)

if(MISSING_HEADERS)
	unset(libhatp_INCLUDE_DIRS)
endif(MISSING_HEADERS)

find_library(libhatp_LIBRARIES ${LIBRARIES} PATH_SUFFIXES lib 
	HINTS ${CURRENT_PREFIX}
	${libhatp_DIR} ${libhatp_ROOT} ${libhatpROOT}
	$ENV{libhatp_DIR} $ENV{libhatp_ROOT} $ENV{libhatpROOT})

if(libhatp_INCLUDE_DIRS AND libhatp_LIBRARIES)
	set(libhatp_FOUND TRUE)
else(libhatp_INCLUDE_DIRS AND libhatp_LIBRARIES)
	if(NOT libhatp_FIND_QUIETLY)
		if(NOT libhatp_INCLUDE_DIRS)
			message(STATUS "Unable to find libhatp, the following header files are missing : ${MISSING_HEADERS}")
		else(NOT libhatp_INCLUDE_DIRS)
			message(STATUS "Header files found.")
		endif(NOT libhatp_INCLUDE_DIRS)
		if(NOT libhatp_LIBRARIES)
			message(STATUS "Unable to find libhatp library files!")
		else(NOT libhatp_LIBRARIES)
			message(STATUS "Library files found.")
		endif(NOT libhatp_LIBRARIES)
	endif(NOT libhatp_FIND_QUIETLY)
endif(libhatp_INCLUDE_DIRS AND libhatp_LIBRARIES)

if(libhatp_FOUND)
	if(NOT libhatp_FIND_QUIETLY)
		message(STATUS "Found components for libhatp")
		message(STATUS "libhatp_INCLUDE_DIRS = ${libhatp_INCLUDE_DIRS}")
		message(STATUS "libhatp_LIBRARIES = ${libhatp_LIBRARIES}")
	endif(NOT libhatp_FIND_QUIETLY)
else(libhatp_FOUND)
	if(libhatp_FIND_REQUIRED)
		set(libhatp_DIR "" CACHE PATH "Paths where to additionally look for
		libhatp (alias of libhatp_ROOT and libhatpROOT)")
		set(libhatp_ROOT "" CACHE PATH "Paths where to additionally look for
		libhatp (alias of libhatp_DIR and libhatpROOT)")
		set(libhatpROOT "" CACHE PATH "Paths where to additionally look for
		libhatp (alias of libhatp_DIR and libhatp_ROOT)")
		if(NOT libhatp_FIND_QUIETLY)
			message("\nCould not find libhatp!\n")
			message("You can use any of the following variables to help CMake to find the header files and libraries :")
			message("	libhatp_DIR or libhatp_ROOT or libhatpROOT")
			message("You should use one and set it to the root path of your local libhatp install directory (if you installed it in a different folder than system root).")
			message("To set one you can either :")
			message("	- use 'set(libhatp_DIR ....)' command in the CMakeLists.txt (separator ' ')")
			message("	- use '-Dlibhatp_DIR=....' as an option to Cmake (separator ';')")
			message("	- set one as an environment variable (e.g. export in bash, with separator ';')\n")
		endif(NOT libhatp_FIND_QUIETLY)
		message(FATAL_ERROR "Could not find libhatp!")
	endif(libhatp_FIND_REQUIRED)
endif(libhatp_FOUND)

mark_as_advanced (
	libhatp_FOUND
	libhatp_LIBRARIES
	libhatp_INCLUDE_DIRS
)
