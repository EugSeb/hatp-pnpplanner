#if !defined HATP_REQUESTLEXER_HH_
#define HATP_REQUESTLEXER_HH_


#include <boost/regex.hpp>

// ---------- String tools ----------
//I HAVE COMMENTED THIS PART
// static std::string& str_replace(const std::string &search, const std::string &replace, std::string &subject) {
//     std::string buffer;
//     int sealeng = search.length();
//     int strleng = subject.length();
//     if (sealeng==0) return subject;
//     for(int i=0, j=0; i<strleng; j=0 ) {
//         while (i+j<strleng && j<sealeng && subject[i+j]==search[j]) j++;
//         if (j==sealeng) {buffer.append(replace); i+=sealeng;}
//         else buffer.append( &subject[i++], 1);
//     }
//     subject = buffer;
//     return subject;
// }

// static std::string& removeFormatting(std::string &s) {
// 	return str_replace(" ", "", s);
// }

// ---------- Default parsing ----------

static const std::string HSPACE		= "[[:space:]]";
static const std::string HWORD		= "([[:word:]]+)";
static const std::string HLPAREN		= "[(]";
static const std::string HRPAREN		= "[)]";
static const std::string HLLIST		= "[(.][[.period.]]" + HSPACE;
static const std::string HRLIST		= "[[.period.]][)]";
static const std::string HCLIST		= "(([[:word:]]+\\s)*)";
static const std::string HLIST		= HLLIST + HCLIST + HRLIST;

static const std::string HLSCOPE		= "[{]";
static const std::string HRSCOPE		= "[}]";
static const std::string HLBRAC		= "[[.left-square-bracket.]]";
static const std::string HRBRAC		= "[[.right-square-bracket.]]";
static const std::string HJSEP		= "[[.colon.]]";
static const std::string HCOMMA		= "[[.comma.]]";
static const std::string HQUOTE		= "[[.quotation-mark.]]";
static const std::string HSTRING		= HQUOTE + HWORD + HQUOTE;
static const std::string HNUMBER		= "([[:digit:]]+)";
static const std::string HSLIST		= HLBRAC + "((("+HSTRING+")|(" + HSTRING + HCOMMA + "))*)" + HRBRAC;

// ---------- Supervision request parsing ----------

static const std::string REQUEST_PLAN	= "plan";
static const std::string REQUEST_FIRST_PLAN	= "firstPlan";
static const std::string REQUEST_REPLAN	= "replan";
static const std::string REQUEST_TASKS	= "getTasks";
static const std::string REQUEST_ACTIONS	= "getActions";
static const std::string REQUEST_ENTITIES	= "getEntities";
static const std::string REQUEST_MTIME	= "setMaxTime";
static const std::string REQUEST_UMTIME	= "unsetMaxTime";
static const std::string RR_UNDEFINED	= "UNDEFINED";
static const std::string RR_WCHANGE		= "WORLD_CHANGE";
static const std::string RR_NEXTPLAN		= "NEXT_PLAN";
static const std::string RR_AGENTWORK	= "AGENT_NOT_WORK";
static const std::string RR_UNDSTATE		= "UNDESIRABLE_STATE";

static const std::string HK_HATPREQ	= HQUOTE + "HATP-REQ" + HQUOTE;
static const std::string HK_REQTYPE	= HQUOTE + "REQ-TYPE" + HQUOTE;
static const std::string HK_REQID	= HQUOTE + "REQ-ID" + HQUOTE;
static const std::string HK_TASKNAME	= HQUOTE + "TASK-NAME" + HQUOTE;
static const std::string HK_PARAMS	= HQUOTE + "PARAMETERS" + HQUOTE;
static const std::string HK_PLANID	= HQUOTE + "PLAN-ID" + HQUOTE;
static const std::string HK_TASKID	= HQUOTE + "TASK-ID" + HQUOTE;
static const std::string HK_REASON	= HQUOTE + "REASON" + HQUOTE;
static const std::string HK_COMMAND	= HQUOTE + "COMMAND" + HQUOTE;
static const std::string HK_EXIT		= HQUOTE + "EXIT" + HQUOTE;
static const std::string HK_MAXTIME	= HQUOTE + "MAXTIME" + HQUOTE;

static const std::string HREQTYPE	= HK_REQTYPE + HJSEP + HSTRING;
static const std::string HREQID		= HK_REQID + HJSEP + HNUMBER;
static const std::string HTASKNAME	= HK_TASKNAME + HJSEP + HSTRING;
static const std::string HPARAMS		= HK_PARAMS + HJSEP + HSLIST;
static const std::string HPLANID		= HK_PLANID + HJSEP + HNUMBER;
static const std::string HTASKID		= HK_TASKID + HJSEP + HNUMBER;
static const std::string HREASON		= HK_REASON + HJSEP + HSTRING;
static const std::string HMAXTIME	= HK_MAXTIME + HJSEP + HNUMBER;

static const std::string SVREQ		= HLSCOPE + HK_HATPREQ + HJSEP + HLSCOPE + HREQTYPE;
static const std::string TIMEREQ		= HLSCOPE + HK_HATPREQ + HJSEP + HLSCOPE + HREQTYPE + HCOMMA + HMAXTIME + HRSCOPE + HRSCOPE;
static const std::string UNSETTIMEREQ	= HLSCOPE + HK_HATPREQ + HJSEP + HLSCOPE + HREQTYPE + HRSCOPE + HRSCOPE;
static const std::string PLANREQ 	= HLSCOPE + HK_HATPREQ + HJSEP + HLSCOPE + HREQTYPE + HCOMMA + HREQID + HCOMMA + HTASKNAME + HCOMMA + HPARAMS + HRSCOPE
								  + HRSCOPE;
static const std::string REPLREQ 	= HLSCOPE + HK_HATPREQ + HJSEP + HLSCOPE + HREQTYPE + HCOMMA + HREQID + HCOMMA + HPLANID + HCOMMA + HTASKID + HCOMMA
								  + HREASON + HCOMMA + HPARAMS + HRSCOPE + HRSCOPE;
static const std::string EXIT 		= HLSCOPE + HK_COMMAND + HJSEP + HK_EXIT + HRSCOPE;


// ---------- REGULAR EXPRESSION  ----------

static const boost::regex SupervisionRequest(SVREQ);
static const boost::regex PlanRequest(PLANREQ);
static const boost::regex ReplanRequest(REPLREQ);
static const boost::regex MaxTimeRequest(TIMEREQ);
static const boost::regex UnsetMaxTimeRequest(UNSETTIMEREQ);
static const boost::regex EXITRequest(EXIT);

#endif
