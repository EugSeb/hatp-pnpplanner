# Find the hatponboard-lib module (this file is meant to be installed using make install)
# Once found the following variables are set :
#	hatponboard-lib_FOUND	True to specify that the module has been found
#
#	hatponboard-lib_INCLUDE_DIRS	Path(s) to the headers
#
#	hatponboard-lib_LIBRARIES	Link these to use hatponboard-lib

# To help CMake in finding the hatponboard-lib module
# one can set the following variables :
# hatponboard-lib_DIR	Those variables can be either set using 'set' command of
# or			CMake, or using '-D<var>=value' while calling CMake or 
# hatponboard-lib_ROOT	finally they can be set as environment variables.
# or			Those three variables are aliases, so just set one is enough.
# hatponboard-libROOT	
#
# You can set any of them using one of the following methods :
#	1) Set it as environnement varialbe (e.g. export)
#	2) Give it as argument for cmake : cmake -Dhatponboard-lib_DIR="...."
#	3) In a CMakeLists.txt, set a variable : set(hatponboard-lib_DIR ...)

#Varaibles to change if the module changes
set(HEADER_FILES msgTypesTrait.hh;wait.hh;tools.hh;VerboseLevel.hh;PlanSender.hh;MainSessionMsg.hh;HATPexception.hh;TimeProjection.hh;TaskListNode.hh;Substitution.hh;WastedTimeData.hh;UndesirableStateData.hh;SocialRuleData.hh;TaskInstance.hh;PlanTreeTaskListNode.hh;PlanTreeTaskList.hh;PlanTree.hh;TaskList.hh;Plan.hh;SocialRuleBase.hh;WorldBaseAgent.hh;WorldBaseReport.hh;Task.hh;hatpPlanningSession.hh;BeliefMethod.hh;LightWorldBaseAgent.hh;LightEntity.hh;PartialPlanAppraiser.hh;ChoicePoint.hh;WorldBase.hh;RequestLexer.hh;PlanStockManager.hh;HTNBase.hh;ChoiceReminder.hh;AttributeSet.hh;LightAttributeSet.hh;LightWorldBase.hh;WorldBaseUpdater.hh;Entity.hh)
set(INCLUDE_PATH_SUFFIXES include include/hatponboard-lib hatponboard-lib)
set(LIBRARIES hatp-tools;hatp-plan;hatp-planningsession)
set(CURRENT_PREFIX /opt/openrobots)

foreach(header ${HEADER_FILES})
	find_path(${header}_PATH ${header} PATH_SUFFIXES ${INCLUDE_PATH_SUFFIXES}
	HINTS ${CURRENT_PREFIX}
	${hatponboard-lib_DIR} ${hatponboard-lib_ROOT} ${hatponboard-libROOT}
	$ENV{hatponboard-lib_DIR} $ENV{hatponboard-lib_ROOT} $ENV{hatponboard-libROOT})
	if(${header}_PATH)
		list(APPEND hatponboard-lib_INCLUDE_DIRS ${${header}_PATH})
		unset(${header}_PATH CACHE)
	else(${header}_PATH)
		list(APPEND MISSING_HEADERS ${header})
	endif(${header}_PATH)
endforeach()

if(hatponboard-lib_INCLUDE_DIRS)
	list(REMOVE_DUPLICATES hatponboard-lib_INCLUDE_DIRS)
endif(hatponboard-lib_INCLUDE_DIRS)

if(MISSING_HEADERS)
	unset(hatponboard-lib_INCLUDE_DIRS)
endif(MISSING_HEADERS)

foreach(library ${LIBRARIES})
	find_library(${library}_PATH ${library} PATH_SUFFIXES lib
	HINTS ${CURRENT_PREFIX}
	${hatponboard-lib_DIR} ${hatponboard-lib_ROOT} ${hatponboard-libROOT}
	$ENV{hatponboard-lib_DIR} $ENV{hatponboard-lib_ROOT} $ENV{hatponboard-libROOT})
	if(${library}_PATH)
		list(APPEND hatponboard-lib_LIBRARIES ${${library}_PATH})
		unset(${library}_PATH CACHE)
	else(${library}_PATH)
		list(APPEND MISSING_LIBRARIES ${library})
	endif(${library}_PATH)
endforeach()

if(hatponboard-lib_LIBRARIES)
	list(REMOVE_DUPLICATES hatponboard-lib_LIBRARIES)
endif(hatponboard-lib_LIBRARIES)

if(MISSING_LIBRARIES)
	unset(hatponboard-lib_LIBRARIES)
endif(MISSING_LIBRARIES)

if(hatponboard-lib_INCLUDE_DIRS AND hatponboard-lib_LIBRARIES)
	set(hatponboard-lib_FOUND TRUE)
else(hatponboard-lib_INCLUDE_DIRS AND hatponboard-lib_LIBRARIES)
	if(NOT hatponboard-lib_FIND_QUIETLY)
		if(NOT hatponboard-lib_INCLUDE_DIRS)
			message(STATUS "Unable to find hatponboard-lib, the following header files are missing : ${MISSING_HEADERS}")
		else(NOT hatponboard-lib_INCLUDE_DIRS)
			message(STATUS "Header files found.")
		endif(NOT hatponboard-lib_INCLUDE_DIRS)
		if(NOT hatponboard-lib_LIBRARIES)
			message(STATUS "Unable to find hatponboard-lib libraries, the following are missing : ${MISSING_LIBRARIES}")
		else(NOT hatponboard-lib_LIBRARIES)
			message(STATUS "Library files found.")
		endif(NOT hatponboard-lib_LIBRARIES)
	endif(NOT hatponboard-lib_FIND_QUIETLY)
endif(hatponboard-lib_INCLUDE_DIRS AND hatponboard-lib_LIBRARIES)

if(hatponboard-lib_FOUND)
	if(NOT hatponboard-lib_FIND_QUIETLY)
		message(STATUS "Found components for hatponboard-lib")
		message(STATUS "hatponboard-lib_INCLUDE_DIRS = ${hatponboard-lib_INCLUDE_DIRS}")
		message(STATUS "hatponboard-lib_LIBRARIES = ${hatponboard-lib_LIBRARIES}")
	endif(NOT hatponboard-lib_FIND_QUIETLY)
else(hatponboard-lib_FOUND)
	if(hatponboard-lib_FIND_REQUIRED)
		set(hatponboard-lib_DIR "" CACHE PATH "Paths where to additionally look for
		hatponboard-lib (alias of hatponboard-lib_ROOT and hatponboard-libROOT)")
		set(hatponboard-lib_ROOT "" CACHE PATH "Paths where to additionally look for
		hatponboard-lib (alias of hatponboard-lib_DIR and hatponboard-libROOT)")
		set(hatponboard-libROOT "" CACHE PATH "Paths where to additionally look for
		hatponboard-lib (alias of hatponboard-lib_DIR and hatponboard-lib_ROOT)")
		if(NOT hatponboard-lib_FIND_QUIETLY)
			message("\nCould not find hatponboard-lib!\n")
			message("You can use any of the following variables to help CMake to find the header files and libraries :")
			message("	hatponboard-lib_DIR or hatponboard-lib_ROOT or hatponboard-libROOT")
			message("You should use one and set it to the root path of your local hatponboard-lib install directory (if you installed it in a different folder than system root).")
			message("To set one you can either :")
			message("	- use 'set(hatponboard-lib_DIR ....)' command in the CMakeLists.txt (separator ' ')")
			message("	- use '-Dhatponboard-lib_DIR=....' as an option to Cmake (separator ';')")
			message("	- set one as an environment variable (e.g. export in bash, with separator ';')\n")
		endif(NOT hatponboard-lib_FIND_QUIETLY)
		message(FATAL_ERROR "Could not find hatponboard-lib!")
	endif(hatponboard-lib_FIND_REQUIRED)
endif(hatponboard-lib_FOUND)

mark_as_advanced (
	hatponboard-lib_FOUND
	hatponboard-lib_LIBRARIES
	hatponboard-lib_INCLUDE_DIRS
)
